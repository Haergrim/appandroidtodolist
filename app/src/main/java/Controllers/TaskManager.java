package Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import Models.SQLiteDb;
import Models.Task;

public class TaskManager {

    private static final int versionDb = 1;
    private static final String nameDb = "ToDo.db";

    private static final String tableTask = "tasks";
    private static final String colId = "Id";
    private static final int numColId = 0;
    private static final String colTitle = "Title";
    private static final int numColTitle = 1;
    private static final String colDescription = "Description";
    private static final int numColDescription = 2;
    private static final String colPriority = "Priority";
    private static final int numColPriority = 3;
    private static final String colCreationDate = "CreationDate";
    private static final int numColCreationDate = 4;
    private static final String colChecked = "Checked";
    private static final int numColChecked = 5;

    private SQLiteDatabase db;

    private SQLiteDb myDb;

    public TaskManager(Context context){
        //On crée la BDD et sa table
        myDb = new SQLiteDb(context, nameDb, null, versionDb);
    }

    public void open(){
        //on ouvre la BDD en écriture
        db = myDb.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        db.close();
    }

    public SQLiteDatabase getBDD(){
        return db;
    }

    public long insertTask(Task task){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(colTitle, task.getTitle());
        values.put(colDescription, task.getDescription());
        values.put(colPriority, task.getPriority());
        values.put(colCreationDate, task.getCreationDate());
        values.put(colChecked, task.getChecked());
        //on insère l'objet dans la BDD via le ContentValues
        return db.insert(tableTask, null, values);
    }

    public int updateTask(int id, Task task){
        //La mise à jour d'une task dans la BDD fonctionne plus ou moins comme une insertion
        //il faut simplement préciser quelle task on doit mettre à jour grâce à l'ID
        ContentValues values = new ContentValues();
        values.put(colTitle, task.getTitle());
        values.put(colDescription, task.getDescription());
        values.put(colPriority, task.getPriority());
        values.put(colCreationDate, task.getCreationDate());
        values.put(colChecked, task.getChecked());
        return db.update(tableTask, values, colId+ " = " + id, null);
    }

    public int removeTaskById(int id){
        //Suppression d'une task de la BDD grâce à l'ID
        return db.delete(tableTask, colId + " = " +id, null);
    }

    public Task getTaskByTitle(String title){
        //Récupère dans un Cursor les valeurs correspondants à une task contenue dans la BDD (ici on sélectionne la task grâce à son titre)
        Cursor c = db.query(tableTask, new String[] {colId, colTitle, colDescription, colPriority}, colTitle+ " LIKE \"" + title +"\"", null, null, null, null);
        return cursorToTask(c);
    }

    public List<Task> getAllTasks(){
        //On remonte la liste de toutes les tâches, on pourrait imaginer ne remonter qu'une partie des tâches déjà effectuées, selon leur date de création par exemple ... sur les 10 derniers jours par exemple.
        //La durée pourrait être paramétrable.
        Cursor c = db.rawQuery("SELECT * from tasks ORDER BY Checked asc, Priority, CreationDate",null);
        //Cursor c = db.query(tableTask, new String[] {colId, colTitle, colDescription, colPriority}, null, null, null, null, colPriority);
        return cursorToTasks(c);
    }

    //Cette méthode permet de convertir un cursor en une task
    private Task cursorToTask(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //On créé une Task
        Task task = new Task();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        task.setId(c.getInt(numColId));
        task.setTitle(c.getString(numColTitle));
        task.setDescription(c.getString(numColDescription));
        task.setPriority(c.getInt(numColPriority));
        task.setCreationDate(c.getString(numColCreationDate));
        task.setChecked(c.getInt(numColChecked));
        //On ferme le cursor
        c.close();

        //On retourne la task
        return task;
    }

    //Cette méthode permet de convertir un cursor en une liste de tasks
    private List<Task> cursorToTasks(Cursor c){

        List<Task> tasks = new ArrayList<>();

        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {

                Task task = new Task();
                //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
                task.setId(c.getInt(numColId));
                task.setTitle(c.getString(numColTitle));
                task.setDescription(c.getString(numColDescription));
                task.setPriority(c.getInt(numColPriority));
                task.setCreationDate(c.getString(numColCreationDate));
                task.setChecked(c.getInt(numColChecked));

                tasks.add(task);
                c.moveToNext();
            }
        }
        c.close();

        return tasks;
    }
}
