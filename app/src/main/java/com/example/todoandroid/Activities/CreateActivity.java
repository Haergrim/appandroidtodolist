package com.example.todoandroid.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.todoandroid.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CreateActivity extends AppCompatActivity {

    EditText titleField;
    EditText descriptionField;
    EditText priorityField;
    Button createButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        titleField = findViewById(R.id.taskTitle);
        descriptionField = findViewById(R.id.taskDescription);
        priorityField = findViewById(R.id.taskPriority);

        createButton = findViewById(R.id.taskCreateValidate);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (titleField.getText().toString() == "") {

                    Snackbar.make(findViewById(R.id.activity_create), "Titre obligatoire", Snackbar.LENGTH_LONG).show();
                }
                else {

                    Intent intent = new Intent(CreateActivity.this ,MainActivity.class);
                    intent.putExtra("taskTitle",titleField.getText().toString());
                    intent.putExtra("taskDescription",descriptionField.getText().toString());
                    intent.putExtra("taskPriority",Integer.parseInt(priorityField.getText().toString()));
                    intent.putExtra("taskCreationDate", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime()));
                    intent.putExtra("taskChecked",0);

                    startActivity(intent);
                }
            }
        });
    }
}
