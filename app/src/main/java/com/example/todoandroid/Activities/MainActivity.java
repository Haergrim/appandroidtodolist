package com.example.todoandroid.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.todoandroid.R;

import java.util.List;

import Controllers.TaskManager;
import Models.CustomAdapter;
import Models.Task;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        final ListView mainList = findViewById(R.id.mainList);
        setSupportActionBar(toolbar);

        //Création d'une instance de ma classe TaskManager
        final TaskManager taskManager = new TaskManager(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( MainActivity.this ,CreateActivity.class);
                startActivity(intent);
            }
        });

        //Récupération des infos passées depuis une autre page s'il y en a
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("taskTitle")) {

                //Création d'une task
                Task task = new Task(
                        getIntent().getExtras().getString("taskTitle"),
                        getIntent().getExtras().getString("taskDescription"),
                        getIntent().getIntExtra("taskPriority",0),
                        getIntent().getExtras().getString("taskCreationDate"),
                        getIntent().getIntExtra("taskChecked",0)
                );

                //On ouvre la base de données pour écrire dedans
                taskManager.open();
                //On insère la task que l'on vient de créer
                taskManager.insertTask(task);

                taskManager.close();

                //SnackBar d'info pour indiquer que la task est bien créée
                Snackbar.make(
                       findViewById(R.id.mainActivity),
                       "Tâche créée",
                       Snackbar.LENGTH_LONG
                ).show();
            }
        }

        taskManager.open();

        //on extrait les tasks de la BDD
        List<Task> tasks = taskManager.getAllTasks();

        taskManager.close();

        if (tasks != null) {
            // Utilisation d'un adapter custom

            final CustomAdapter adapter = new CustomAdapter(MainActivity.this,
                    R.layout.list_rows, tasks);


            mainList.setAdapter(adapter);

            // Set an item click listener for ListView
            mainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    // Récupération de l'objet cliqué
                    Task selectedItem = (Task) parent.getItemAtPosition(position);

                    // On modifie son statut
                    if(selectedItem.getChecked() == 0) {
                        selectedItem.setChecked(1);
                    }
                    else {
                        selectedItem.setChecked(0);
                    }

                    // Update en base de données
                    taskManager.open();

                    taskManager.updateTask(selectedItem.getId(),selectedItem);

                    taskManager.close();

                    //Restart l'activité, c'est un peu bourrin mais j'ai pas trouvé d'autre solution pour l'instant
                    //On vide l'intent au cas où on vient de la création pour éviter de le créer en double
                    finish();
                    overridePendingTransition(0, 0);
                    getIntent().removeExtra("taskTitle");
                    getIntent().removeExtra("taskDescription");
                    getIntent().removeExtra("taskPriority");
                    getIntent().removeExtra("taskCreationDate");
                    getIntent().removeExtra("taskChecked");
                    startActivity(getIntent());
                    overridePendingTransition(0, 0);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
