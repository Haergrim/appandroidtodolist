package Models;

public class Task {

    private int id;
    private String title;
    private String description;
    private int priority;
    private String creationDate;
    private int checked;

    public Task() {
    }

    public Task(String title, String description, int priority, String creationDate, int checked) {
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.creationDate = creationDate;
        this.checked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

    public String toString(){
        return "Titre : "+title+"\nDescription : "+description +"\nPriorite : "+priority +"\nDate de creation : "+creationDate;
    }
}
