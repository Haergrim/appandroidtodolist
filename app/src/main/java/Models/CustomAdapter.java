package Models;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.todoandroid.R;

import java.util.List;

public class CustomAdapter extends ArrayAdapter<Task> {

    private int resourceLayout;
    private Context mContext;

    public CustomAdapter(Context context, int resource, List<Task> items) {
        super(context, resource, items);
        this.resourceLayout = resource;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        Task p = getItem(position);

        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.row);

            if (tt1 != null) {
                tt1.setText(p.toString());
                if(p.getChecked() == 1) {
                    tt1.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                }
                else {
                    tt1.setPaintFlags(0);
                }
            }
        }

        return v;
    }

}