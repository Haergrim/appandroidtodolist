package Models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class SQLiteDb extends SQLiteOpenHelper {

    private static final String tableTask = "tasks";
    private static final String colId = "Id";
    private static final String colTitle = "Title";
    private static final String colDescription = "Description";
    private static final String colPriority = "Priority";
    private static final String colCreationDate = "CreationDate";
    private static final String colChecked = "Checked";

    private static final String CREATE_BDD = "CREATE TABLE " + tableTask + " ("
            + colId + " INTEGER PRIMARY KEY AUTOINCREMENT, " + colTitle + " TEXT NOT NULL, "
            + colDescription + " TEXT," + colPriority + " INTEGER, " + colCreationDate + " TEXT, " + colChecked + " INTEGER );";

    public SQLiteDb(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //on crée la table à partir de la requête écrite dans la variable CREATE_BDD
        db.execSQL(CREATE_BDD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //On peut faire ce qu'on veut ici moi j'ai décidé de supprimer la table et de la recréer
        //comme ça lorsque je change la version les id repartent de 0
        db.execSQL("DROP TABLE " + tableTask + ";");
        onCreate(db);
    }
}
